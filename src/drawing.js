import { fromEvent } from 'rxjs';
import { switchMap, takeUntil, map, throttleTime, pairwise } from  'rxjs/operators'

const canvas = document.getElementById('canvas');
const context = canvas.getContext('2d');
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

const drawLine = points => {
    context.beginPath();
    context.moveTo(points[0].x, points[0].y);
    context.lineTo(points[1].x, points[1].y);
    context.stroke()
}

const mouseUp$ = fromEvent(canvas, 'mouseup');

const mouseMove$ = fromEvent(canvas, 'mousemove').pipe(
    throttleTime(100),
    takeUntil(mouseUp$),
    map(e => ({ x: e.clientX, y: e.clientY })),
    pairwise()
);

const mouseDown$ = fromEvent(canvas, 'mousedown').pipe(
    switchMap(() => mouseMove$)
)

mouseDown$.subscribe(drawLine)

