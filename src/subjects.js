import { Subject, BehaviorSubject, ReplaySubject } from 'rxjs';

document.addEventListener('click', () => {
    //const stream$ = new Subject();
    //const stream$ = new BehaviorSubject('first');
    const stream$ = new ReplaySubject(2); //  воспроизводит не выполненные next

    stream$.next('hello');
    stream$.next('rx');
    stream$.next('js');

    stream$.subscribe(v => console.log('Value:', v))
})